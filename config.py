import os
import sys
from warnings import warn

class BaseConfig:

    environments = {}

    DEFAULT_ENV_NAME = 'default'

    def __init__(self):

        # The path of the class that extends BaseConfig is
        # where environment indicator files may be found,
        # e.g. _env.production indicating 'production' environment.
        self._configPath = os.path.dirname(
            sys.modules[self.__module__].__file__)

        # The current environment
        self._initializeRackEnvironment()

    def get(self, setting):
        value = self.environments[self.rackEnvironment].get(setting)
        if not value:
            value = self.environments[self.DEFAULT_ENV_NAME].get(setting)
            if not value:
                raise Exception('Setting "%s" was not found in your configuration for environment %s' 
                                % (setting, self.rackEnvironment))
        return value

    def _initializeRackEnvironment(self):
        RACK_ENV = None
        RACK_ENV = self._getRackEnvironmentFromFiles()
        if not RACK_ENV:
            RACK_ENV = self._getRackEnvironmentFromEnvironmentVariable()
        if not RACK_ENV:
            RACK_ENV = self._getRackEnvironmentBecauseOnlyChoice()
        if not RACK_ENV:
            if self.environments:
                raise Exception('Could not determine environment to select config settings')
            else:
                warn('Environment is not set. Config settings are not available.')

        self.rackEnvironment = RACK_ENV

    def _getRackEnvironmentBecauseOnlyChoice(self):
        # If only 1 environment is defined, select it by default
        if len(self.environments) == 1:
            return self.environments.keys()[0]
        else:
            return None

    def _getRackEnvironmentFromFiles(self):
        RACK_ENV = None
        for environment in self.environments:
            if os.path.isfile(os.path.join(self._configPath,
                                       '_env.'+environment)):
                if RACK_ENV:
                    raise Exception(
                        'Multiple valid environments were indicated by _env.* files. '
                        'Only one environment is allowed.')
                else:
                    RACK_ENV = environment
        return RACK_ENV

    def _getRackEnvironmentFromEnvironmentVariable(self):
        RACK_ENV = os.getenv('RACK_ENV')
        if RACK_ENV:
            if RACK_ENV in self.environments:
                return RACK_ENV
            else:
                raise Exception(
                    'Could not recognize RACK_ENV: "%s". '
                    'Available environments are: "%s". '
                    % (RACK_ENV, self.environments.keys())
                    )

    def _getAllSettings(self):
        allSettings = set()
        for environment in self.environments:
            allSettings = allSettings.union(
                self.environments[environment].keys())
        return allSettings
