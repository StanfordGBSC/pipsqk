import os

class AbstractJobManager(object):

    _fileExtension = None

    def writeToFile(cls, segment):
        text = cls._renderSegmentToText(segment)
        filename = cls._getFileName(segment)
        directory = os.path.dirname(filename)
        if directory and not os.path.exists(directory):
            os.makedirs(directory)
        with open(filename, 'w') as f:
            f.write(text)

    def _getFileName(cls, segment):
        directory = segment.getAttribute('directory')
        name = segment.getAttribute('name')
        if cls._fileExtension:
            suffix = '.'+cls._fileExtension
        else:
            suffix = ''

        if directory:
            return os.path.join(directory, name+suffix)
        else:
            return name+suffix
