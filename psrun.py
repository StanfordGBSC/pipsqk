#!/usr/bin/env python

from optparse import OptionParser

from pipsqk.components import SuperPipe

class PipSQK(object):
   def __init__(self, options=None):
      if not options:
         options = self._parse_commandline_args()
      self._options = options

      self._project = SuperPipe.load(self._options.project)

   def _parse_commandline_args(self):

      parser = OptionParser()

      parser.add_option(
         "-p", 
         "--project", 
         dest="project",
         help="pipsqk project path and filename")

      parser.add_option(
         "--l",
         "--pipeline",
         dest="pipeline",
         help="pipsqk pipeline name")

      parser.add_option(
         "-c", 
         "--command", 
         dest="command",
         help="pipsqk command")

      (options, args) = parser.parse_args()

      self._clean_args(args)
      return self._clean_options(options)

   def _clean_args(self, args):
      # No args expected
      if len(args):
         raise Exception('Unexpected argument(s): "%s"', args)

   def _clean_options(self, options):
      # TODO
      return options
      
   def runCommand(self):
      commandList = {
         'status': self._status,
         'run': self._run,
         'kill': self._kill,
         'resume': self._resume,
      }

      run = commandList.get(self._options.command)
      if not run:
         raise Exception('Could not interpret command "%s"' 
                         % self._options.command)
      run()

   def _status(self):
      print self._project

   def _run(self):
      if self._options.pipeline:
         for pipeline in self._project._children:
            if pipeline.getAttribute('name') == self._options.pipeline:
               pipeline.run()
      else:
         self._project.run()

   def _kill(self):
      print "Todo"

   def _resume(self):
      print "Todo"

if __name__=='__main__':
   PipSQK().runCommand()
