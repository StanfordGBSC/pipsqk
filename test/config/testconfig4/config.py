from pipsqk import config

class Config(config.BaseConfig):

    environments = {
        'default': {
            'setting1': 'default',
        },
        'env1' : {
            'setting1': 'env1',
            'setting2': 'env1',
        },
        
        'env2' : {
            'setting2': 'env2',
        },
    }
