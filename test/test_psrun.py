#!/usr/bin/env python

import os
import subprocess
import tempfile
import time
import unittest

from pipsqk.components import Step, Pipeline, SuperPipe
from pipsqk.psrun import PipSQK

class TestPipSQK(unittest.TestCase):

    def testRunSegment(self):
        tempdir = tempfile.mkdtemp()

        (P, outputFile1, outputFile2) = self.pipelineSetup(tempdir)
        pipsqkFile = P._getPipsqkFile()
        P.save()

        class Options(object):
            pass

        opt = Options()

        opt.command = 'run'
        opt.project = pipsqkFile
        opt.pipeline = 'segment2'

        pipsqkrun = PipSQK(opt)

        pipsqkrun.runCommand()

        self.pollForFile(outputFile2)

        # Verify that the file from the other segment was never generated
        devnull = open('/dev/null', 'w')
        cmd = ['ls', outputFile1]
        self.assertNotEqual(subprocess.call(cmd, stderr=devnull, stdout=devnull), 0)
        devnull.close()

    def testRunPipeline(self):
        tempdir = tempfile.mkdtemp()

        (P, outputFile1, outputFile2) = self.pipelineSetup(tempdir)
        pipsqkFile = P._getPipsqkFile()
        P.save()

        class Options(object):
            pass

        opt = Options()

        opt.command = 'run'
        opt.project = pipsqkFile
        opt.pipeline = None

        pipsqkrun = PipSQK(opt)

        pipsqkrun.runCommand()

        self.pollForFile(outputFile1)
        self.pollForFile(outputFile2)

    def pipelineSetup(self, tempdir):
        # Make a generic pipeline
        outputFile1 = os.path.join(tempdir, 'text1.out')
        commandA = 'echo Hello >> %s' % outputFile1
        A = Step(command=commandA)
        S1 = Pipeline([A], name='segment1')
        
        outputFile2 = os.path.join(tempdir, 'text2.out')
        commandB = 'echo Hello >> %s' % outputFile2
        B = Step(command=commandB)
        S2 = Pipeline([B], name='segment2')

        superpipe = SuperPipe([S1, S2], directory=tempdir, name='test', jobManager='bpipe')

        return (superpipe, outputFile1, outputFile2)

    def pollForFile(self, filepath):
        checkIfResultsAreReady = ['ls', filepath]
        devnull = open('/dev/null', 'w')
        timeout = 10 # seconds
        tic = time.time()
        while subprocess.call(checkIfResultsAreReady, stderr=devnull, stdout=devnull) != 0:
            if time.time() - tic > timeout:
                raise (Exception('Timed out while polling for file'))
            time.sleep(0.1)
        devnull.close()

if __name__ == '__main__':
    unittest.main()
