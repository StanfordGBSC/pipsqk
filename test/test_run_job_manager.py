#!/usr/bin/env python

import os
import subprocess
import tempfile
import time
import unittest

from pipsqk.components import SuperPipe, Pipeline, Step

class TestSegment(unittest.TestCase):

    def setUp(self):
        self.tempdir = tempfile.mkdtemp()
        self.tempfile1 = os.path.join(self.tempdir,'test.txt.tmp')
        self.tempfile2 = os.path.join(self.tempdir,'test.txt')

    def tearDown(self):
        subprocess.call(['rm', '-r', self.tempdir])

    def testRunSegmentSJM(self):
        segment = Pipeline()
        segment.setAttribute(jobManager='sjm')
        self.abstractTestRunSegment(segment)

    def testRunSegmentBpipe(self):
        segment = Pipeline()
        segment.setAttribute(jobManager='bpipe')
        self.abstractTestRunSegment(segment)

    def testRunPipelineSJM(self):
        pipeline = SuperPipe()
        pipeline.setAttribute(jobManager='sjm')
        self.abstractTestRunPipeline(pipeline)

    def testRunPipelineBpipe(self):
        pipeline = SuperPipe()
        pipeline.setAttribute(jobManager='bpipe')
        self.abstractTestRunPipeline(pipeline)

    def abstractTestRunSegment(self, segment):
        # Build and run a simple 2-step segment with a dependency
        # and verify that steps execute in the right order

        segment.setAttribute(directory=self.tempdir)

        helloStep = segment.newStep(
            name='world', 
            command="echo 'Hello World' >> %s" % self.tempfile1,
            )
        helloStep.setAttribute(host='localhost')
        
        finishStep = segment.newStep(name='hello', 
                                      command="mv %s %s" 
                                      % (self.tempfile1, self.tempfile2),
                                      )
        finishStep.setAttribute(host='localhost')
        finishStep.after(helloStep)

        retcode = segment.run()

        self.assertEqual(retcode, 0)

        self.pollForFile(self.tempfile2)

        with open(self.tempfile2, 'r') as f:
            results = ''
            line = f.read()
            while line:
                results += line
                line = f.read()

        self.assertEqual(results, "Hello World\n")

    def abstractTestRunPipeline(self, pipeline):
        # Build and run a a simple 2-segment pipeline with a dependency
        # and verify that steps execute in the right order

        pipeline.setAttribute(directory=self.tempdir)

        helloSegment = pipeline.newSegment(
            [
                Step(
                    name='world', 
                    command="echo 'Hello World' >> %s" % self.tempfile1,
                    host='localhost'
                )
            ]
        )

        finishSegment = pipeline.newSegment(
            [
                Step(
                    name='hello', 
                    command="mv %s %s" % (self.tempfile1, self.tempfile2),
                    host='localhost'
                )
            ]
        )
        
        finishSegment.after(helloSegment)

        retcodeList = pipeline.run()

        self.assertEqual(retcodeList, [0])

        self.pollForFile(self.tempfile2)
        
        with open(self.tempfile2, 'r') as f:
            results = ''
            line = f.read()
            while line:
                results += line
                line = f.read()

        self.assertEqual(results, "Hello World\n")

    def pollForFile(self, filepath, timeout=60):
        start = time.time()
        checkIfResultsAreReady = ['ls', filepath]
        devnull = open('/dev/null', 'w')
        while subprocess.call(checkIfResultsAreReady, stderr=devnull, stdout=devnull) != 0:
            time.sleep(0.1)
            if time.time() - start > timeout:
                raise Exception("Timed out while waiting for file %s" %filepath)
        devnull.close()

if __name__ == '__main__':
    unittest.main()
