#!/usr/bin/env python

import unittest

from pipsqk.job_managers.utils.ordertree import OrderTree
from pipsqk.components import Step

class TestOrderTree(unittest.TestCase):
    def setUp(self):
        # Various dependency patterns for use in tests

        # Linear consecutive
        self.a1 = Step(name='a1')
        self.a2 = Step(name='a2')
        self.a3 = Step(name='a3')
        self.a3.after(self.a2)
        self.a2.after(self.a1)
        self.aGroup = [self.a1, self.a2, self.a3]

        # Independent
        self.b1 = Step(name='b1')
        self.b2 = Step(name='b2')
        self.b3 = Step(name='b3')
        self.bGroup = [self.b1, self.b2, self.b3]

        # d dependent on multiple branches
        self.dleft1 = Step(name='dleft1')
        self.dleft2 = Step(name='dleft2')
        self.dright1 = Step(name='dright1')
        self.dright2 = Step(name='dright2')
        self.d = Step(name='d')
        self.dleft2.after(self.dleft1)
        self.dright2.after(self.dright1)
        self.d.after(self.dright2)
        self.d.after(self.dleft2)
        self.dGroup = [self.dleft1, self.dleft2, self.dright1, self.dright2, self.d]

        # multiple branches dependent on e
        self.e = Step(name='e')
        self.eleft1 = Step(name='eleft1')
        self.eleft2 = Step(name='eleft2')
        self.eright1 = Step(name='eright1')
        self.eright2 = Step(name='eright2')
        self.eleft1.after(self.e)
        self.eleft2.after(self.eleft1)
        self.eright1.after(self.e)
        self.eright2.after(self.eright1)
        self.eGroup = [self.e, self.eleft1, self.eleft2, self.eright1, self.eright2]

        # crazy 8
        self.f = Step(name='f')
        self.fleft1 = Step(name='fleft1')
        self.fleft2 = Step(name='fleft2')
        self.fright1 = Step(name='fright1')
        self.g = Step(name='g')
        self.g1 = Step(name='g1')
        self.h = Step(name='h')
        self.h1 = Step(name='h1')
        self.h2 = Step(name='h2')
        self.i = Step(name='i')

        self.fleft1.after(self.f)
        self.fleft2.after(self.fleft1)
        self.fright1.after(self.f)

        self.h.after(self.fright1)
        self.h1.after(self.h)
        self.h2.after(self.h1)

        self.g.after(self.fleft2)
        self.g.after(self.h)
        self.g1.after(self.g)

        self.i.after(self.h2)
        self.i.after(self.g1)
        self.crazyEightGroup = [self.f, self.fleft1, self.fleft2, self.fright1, 
                                self.g, self.g1, self.h, self.h1, self.h2, self.i]

    def tearDown(self):
        pass

    def testTerminalNode(self):
        root = OrderTree.Node()
        itemList = [
            self.b1,
            ]
        OrderTree._terminalNode(itemList, root)
        
        self.assertEqual(root.op, None)
        self.assertEqual(root.terms[0], self.b1)

    def testTerminalNodeNeg(self):
        root = OrderTree.Node()
        itemList = [
            self.b1,
            self.b2,
            ]
        self.assertRaises(AssertionError,
                  OrderTree._terminalNode,
                  itemList, root)

    def testConsecutiveNode(self):
        root = OrderTree.Node()
        groups = [
            [self.b1],
            [self.b2],
            [self.b3],
            ]
        OrderTree._consecutiveNode(groups, root)
        self.assertEqual(root.op,'+')
        for item in groups:
            self.assertEqual(item, root.terms.pop(0).terms)

    def testConsecutiveNodeNeg(self):
        root = OrderTree.Node()
        groups = [[self.b1]]
        self.assertRaises(AssertionError,
                          OrderTree._consecutiveNode,
                          groups, root)

    def testConcurrentNode(self):
        root = OrderTree.Node()
        groups = [
            [self.b1],
            [self.b2],
            [self.b3],
            ]
        OrderTree._concurrentNode(groups, root)
        self.assertEqual(root.op,',')
        for item in groups:
            self.assertEqual(item, root.terms.pop(0).terms)

    def testConcurrentNodeNeg(self):
        root = OrderTree.Node()
        groups = [
            [self.b1],
            ]
        self.assertRaises(AssertionError,
                          OrderTree._concurrentNode,
                          groups, root)
        
    def testSeparateUnrelatedSteps(self):
        stepList = []
        stepList.extend(self.aGroup) # 3 independent members should be broken up
        stepList.extend(self.bGroup) # all members interdependent
        stepList.extend(self.crazyEightGroup) # all members interdependent
        
        stepListCopy = []
        stepListCopy.extend(stepList)

        groups = OrderTree._separateUnrelatedSteps(stepList)

        # Input obj should not change
        self.assertEqual(stepList, stepListCopy)
        
        # Verify that we broke this into 3 groups with total number of Steps conserved
        self.assertEqual(len(groups), 3 + 1 + 1)

        totalGroupMembers = 0
        for group in groups:
            totalGroupMembers += len(group)
        self.assertEqual(len(stepList), totalGroupMembers)

    def testRemoveLowerBranchMembersA(self):
        # a group is a linear path. All members should go into branch.
        stepList = []
        stepList.extend(self.aGroup)
        branches = OrderTree._removeLowerBranchMembers(stepList)
        for step in self.aGroup:
            self.assertTrue(step in branches)
        self.assertTrue(stepList == [])

    def testRemoveLowerBranchMembersB(self):
        # b group has independent members. They should all be in lower branches.
        # this use case should never occur in source code, since
        # independent groups will first be split into separate subtrees
        stepList = []
        stepList.extend(self.bGroup)
        branches = OrderTree._removeLowerBranchMembers(stepList)
        for step in self.bGroup:
            self.assertTrue(step in branches)
        self.assertTrue(stepList == [])

    def testRemoveLowerBranchMembersD(self):
        # d group is an inverted V where the apex waits on the branches
        # all should be removed except the tip.
        stepList = []
        stepList.extend(self.dGroup)
        branches = OrderTree._removeLowerBranchMembers(stepList)
        for item in [self.dleft1, self.dleft2, self.dright1, self.dright2]:
            self.assertTrue(item in branches)
        self.assertTrue(stepList == [self.d])


    def testRemoveLowerBranchMembersE(self):
        # e group is a V where the apex is first.
        # should remove only the tip e.
        stepList = []
        stepList.extend(self.eGroup)
        branches = OrderTree._removeLowerBranchMembers(stepList)
        self.assertTrue(branches == [self.e])
        for item in [self.eleft1, self.eleft2, self.eright1, self.eright2]:
            self.assertTrue(item in stepList)

    def testRemoveBranch(self):
        # Upside-down V pattern with d at the tip after left and right branch
        allItems = self.dGroup
        remainingItems = []
        remainingItems.extend(allItems)
        leaf = self.dleft1
        remainingItems.remove(leaf)

        leftBranch = OrderTree._removeBranch(leaf, remainingItems, allItems)

        # left branch should be splot. d and right branch remain.
        self.assertTrue(self.dleft1 in leftBranch)
        self.assertTrue(self.dleft2 in leftBranch)
        self.assertTrue(len(leftBranch) == 2)
        self.assertTrue(len(remainingItems) + len(leftBranch) == len(allItems))

        leaf = self.dright1
        remainingItems.remove(leaf)

        rightBranch = OrderTree._removeBranch(leaf, remainingItems, allItems)

        # right branch should be split. d remains.
        self.assertTrue(self.dright1 in rightBranch)
        self.assertTrue(self.dright2 in rightBranch)
        self.assertTrue(len(rightBranch) == 2)
        self.assertEqual(remainingItems, [self.d])
        self.assertEqual(len(remainingItems) + len(leftBranch) + len(rightBranch), len(allItems))

    def testRemoveItemsWithNoPrerequisitesD(self):
        # e is an inverted V pattern. where the tip comes last.
        # This should remove the first step on each leg.
        stepList = []
        stepList.extend(self.dGroup)
        noPrereqs = OrderTree._removeItemsWithNoPrerequisites(stepList)
        self.assertTrue(self.dright1 in noPrereqs)
        self.assertTrue(self.dleft1 in noPrereqs)
        self.assertEqual(len(noPrereqs), 2)
        # Verify that number of items is conserved
        self.assertEqual(len(noPrereqs) + len(stepList), len(self.dGroup))

    def testRenderTree_A(self):
        text = OrderTree(self.aGroup).render()
        self.assertEqual(text, '[a1+a2+a3]')

    def testRenderTree_B(self):
        text = OrderTree(self.bGroup).render()
        self.assertEqual(text, '[b3,b2,b1]')

    def testRenderTree_D(self):
        text = OrderTree(self.dGroup).render()
        self.assertEqual(text, '[[[dleft1+dleft2],[dright1+dright2]]+d]')

    def testRenderTree_E(self):
        text = OrderTree(self.eGroup).render()
        self.assertEqual(text, '[e+[[eleft1+eleft2],[eright1+eright2]]]')

    def testRenderTree_CrazyEight(self):
        text = OrderTree(self.crazyEightGroup).render()
        self.assertEqual(text, '[f+[[[fright1+h],[fleft1+fleft2]]+[[[g+g1],[h1+h2]]+i]]]')

if __name__ == '__main__':
    unittest.main()
